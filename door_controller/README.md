 * When creating the DB, it is a good idea to set the sqlite page
   size to 512 in order to save some memory

 * The circuit board was created with <https://www.kicad.org/>

 * To make the circuit board:
   * <https://www.pcbway.com/>
   * <https://www.allpcb.com/activity/prototype2023.html>
   * In Brazil: <https://pcbbrasil.com.br>

