const char* master_keys[] = {
    // Cleaning staff's blue tag
    "19b20713d9ad41dc020114f5c2101083a1f8791bcc450a3fc2fb2cbee2d560d1",
    // Nelson's fair card
    "d41a773a018d2be6409396a31c7b1a029c0e01f79cf93ee98138f2f49c689369"
};

const char* reboot_keys[] = {
    // Nelson's teacher ID card
    "a00b857141d52efcba976107504d05ce49967ff1e78f9ceed7321e6a6b4cf44d",
};

const char* rollback_keys[] = {
    // Nelson's bus card
    "69010e8d00d36158eb907f4c840093f4b4ccb2812d66ada9e0b83848b7b771db",
};
