#ifndef NETWORK_CONNECTION_H
#define NETWORK_CONNECTION_H

void initWiFi();

void printMAC();

void checkNetConnection();

bool connected();

void netReset();

#endif
