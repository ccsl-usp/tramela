CREATE TABLE "auth" (
    userID    VARCHAR(255)    NOT NULL,
    doorID    VARCHAR(255)    NOT NULL,
    FOREIGN KEY (userID) REFERENCES "users"(id),
    FOREIGN KEY (doorID) REFERENCES "doors"(id)
);

CREATE TABLE "doors" (
    id              INTEGER         NOT NULL,
    location        VARCHAR(255)    NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE "users" (
    id    VARCHAR(255)    NOT NULL,
    name  VARCHAR(255)    NOT NULL,
    email VARCHAR(255)    NOT NULL,
    PRIMARY KEY (id)
);

CREATE index useridx on "auth"(userID, doorID);

CREATE TABLE "groups" (
    id    INTEGER PRIMARY KEY,
    name  VARCHAR(255) NOT NULL
);

CREATE TABLE "user_groups" (
    userID    VARCHAR(255) NOT NULL,
    groupID   INTEGER      NOT NULL,
    FOREIGN KEY (userID) REFERENCES "users"(id),
    FOREIGN KEY (groupID) REFERENCES "groups"(id)
);

CREATE TABLE "group_doors" (
    groupID   INTEGER      NOT NULL,
    doorID    INTEGER      NOT NULL,
    FOREIGN KEY (groupID) REFERENCES "groups"(id),
    FOREIGN KEY (doorID) REFERENCES "doors"(id)
);
