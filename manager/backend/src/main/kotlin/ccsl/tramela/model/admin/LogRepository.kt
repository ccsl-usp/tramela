package ccsl.tramela.model.admin

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AccessRepository : JpaRepository<Access, Long> {}

@Repository
interface SystemRepository : JpaRepository<System, Long> {}
