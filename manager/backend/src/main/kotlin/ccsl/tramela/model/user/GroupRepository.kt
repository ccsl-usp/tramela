package ccsl.tramela.model.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface GroupRepository : JpaRepository<Group, Long> {
    fun findByName(name: String): Group?
}

@Repository
interface UserGroupRepository : JpaRepository<UserGroup, Long> {
    fun findByUserId(userId: String): List<UserGroup>
    fun findByGroupId(groupId: Long): List<UserGroup>
}

@Repository
interface GroupDoorRepository : JpaRepository<GroupDoor, Long> {
    fun findByGroupId(groupId: Long): List<GroupDoor>
    fun findByDoorId(doorId: Int): List<GroupDoor>
}
