package ccsl.tramela.model.user

import jakarta.persistence.*
import jakarta.validation.GroupSequence


data class CreateDoorLockDTO(
    val location: String = "",
    val groups: List<Long>
)

data class DeleteDoorLockDTO(
    val id: Int = 0,
)

data class UpdateDoorLockDTO(
    val id: Int = 0,
    val location: String = "",
    val groups: List<Long>
)

data class ExecuteCommandDTO(
    val recipient: String = "", // door ID or "All"
    val command: String = "",
)

data class DoorDTO (
    val id: Int = 0,
    val location: String = "",
    val groups: List<Long>
)

@Entity
@Table(name = "doors")
data class Door (
    @Id @GeneratedValue(strategy=GenerationType.IDENTITY)
    val id: Int = 0,
    var location: String = "",
)
