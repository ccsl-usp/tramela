package ccsl.tramela.model.user

import jakarta.persistence.*
import java.io.Serializable

@Embeddable
data class AuthId(
    val doorId: Int = 0,
    val userId: String = ""
) : Serializable

@Entity
@Table(name = "auth")
data class Auth(
    @EmbeddedId
    val id: AuthId,

    @ManyToOne(targetEntity = Door::class)
    @MapsId("doorId")
    @JoinColumn(name = "doorId", referencedColumnName = "id")
    val door: Door,

    @ManyToOne(targetEntity = User::class)
    @MapsId("userId")
    @JoinColumn(name = "userId", referencedColumnName = "id")
    val user: User
)
