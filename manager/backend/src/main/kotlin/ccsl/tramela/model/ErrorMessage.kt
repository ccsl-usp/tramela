package ccsl.tramela.model

class ErrorMessageDTO(
    val location: String,
    val message: String,
    val action: String,
) {
    // Default constructor
    constructor() : this("", "", "")
}

