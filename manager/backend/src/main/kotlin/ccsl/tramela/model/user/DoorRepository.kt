package ccsl.tramela.model.user

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DoorRepository: JpaRepository<Door, String> {
    fun existsById(id: Int): Boolean
    fun findById(id: Int): Door?
}
