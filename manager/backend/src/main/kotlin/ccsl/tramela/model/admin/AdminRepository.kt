package ccsl.tramela.model.admin

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface AdminRepository: JpaRepository<Admin, String> {

    fun findByEmail(email: String): Admin?

    fun existsByEmail(email: String): Boolean

    fun findById(id: UUID): Admin?
}
