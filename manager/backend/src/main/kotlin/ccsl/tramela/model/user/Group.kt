package ccsl.tramela.model.user

import jakarta.persistence.*
import java.io.Serializable

data class CreateGroupDTO(
    val name: String,
    val doors: List<Int>
)

data class UpdateGroupDTO(
    val id: Long,
    val name: String,
    val doors: List<Int>
)

data class GroupDTO(
    val id: Long = 0,
    var name: String = "",
    val doors: List<Int> = listOf()
)

data class DeleteGroupDTO(
    val id: Long
)

@Entity
@Table(name = "`groups`")
data class Group(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    var name: String = "",
)

@Embeddable
data class UserGroupId(
    @Column(name = "userId")
    val userId: String,

    @Column(name = "groupId")
    val groupId: Long
) : Serializable

@Entity
@Table(name = "`user_groups`")
data class UserGroup(
    @EmbeddedId
    val id: UserGroupId,

    @ManyToOne()
    @MapsId("userId")
    @JoinColumn(name = "userId")
    var user: User,

    @ManyToOne()
    @MapsId("groupId")
    @JoinColumn(name = "groupId")
    var group: Group
)

@Embeddable
data class GroupDoorId(
    @Column(name = "doorId")
    val doorId: Int,

    @Column(name = "groupId")
    val groupId: Long
) : Serializable

@Entity
@Table(name = "`group_doors`")
data class GroupDoor(
    @EmbeddedId
    val id: GroupDoorId,

    @ManyToOne()
    @MapsId("groupId")
    @JoinColumn(name = "groupId")
    var group: Group,

    @ManyToOne()
    @MapsId("doorId")
    @JoinColumn(name = "doorId")
    var door: Door
)
