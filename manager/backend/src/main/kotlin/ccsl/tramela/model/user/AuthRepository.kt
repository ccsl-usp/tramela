package ccsl.tramela.model.user

import org.springframework.data.jpa.repository.JpaRepository

interface AuthRepository: JpaRepository<Auth, AuthId> {
    fun findByUserId(userId: String): List<Auth>
    fun findByDoorId(doorId: Int): List<Auth>
}
