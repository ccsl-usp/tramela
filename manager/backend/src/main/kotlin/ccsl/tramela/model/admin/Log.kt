package ccsl.tramela.model.admin

import jakarta.persistence.*

data class GetLogDTO(
    val page: Int,
    val size: Int
)

@Entity
@Table(name = "`access`")
data class Access (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    var bootcount: Int = 0,
    var time: String = "",
    var door: Int = 0,
    var reader: Int = 0,
    var authorization: Int = 0,
    var card: Int = 0
)

@Entity
@Table(name = "`system`")
data class System (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    var bootcount: Int = 0,
    var time: String = "",
    var door: Int = 0,
    var message: String = ""
)
