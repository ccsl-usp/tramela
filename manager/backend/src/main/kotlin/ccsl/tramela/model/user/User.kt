package ccsl.tramela.model.user

import jakarta.persistence.*

data class UpdateUserDTO(
    val id: String,
    val name: String,
    val email: String,
    val doors: List<Int>,
    val groups: List<Long>
)

data class CreateUserDTO(
    val id: String,
    val name: String,
    val email: String,
    val doors: List<Int>,
    val groups: List<Long>
)

data class DeleteUserDTO(
    val id: String
)


data class UserDTO (
    val id: String,
    val name: String,
    val email: String,
    val doors: List<Int>,
    val groups: List<Long>
)

@Entity
@Table(name = "users")
data class User (
    @Id
    val id: String = "",
    var name: String = "",
    var email: String = "",
)
