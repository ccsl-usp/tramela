package ccsl.tramela.model.admin

import jakarta.persistence.*
import jakarta.validation.constraints.NotBlank
import java.util.*

data class LoginUserDTO (
    @field:NotBlank(message="Email must not be blank")
    val email: String,
    @field:NotBlank(message="Password must not be blank")
    val password: String
)

data class CreateAdminDTO (
    val email: String,
    val password: String,
    val name: String,
    val imageUri: String?
)

data class AdminInfoDTO(
    val id: UUID,
    val name: String,
    val email: String,
    val imageUri: String?
)

@Entity
@Table(name = "`admin`")
data class Admin (
    @Id
    val id: UUID = UUID.randomUUID(),

    var email: String = "",
    var password: String? = null,
    var name: String = "",
    var imageUri: String? = null
)
