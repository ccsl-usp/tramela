package ccsl.tramela.controller

import ccsl.tramela.model.MessageDTO
import ccsl.tramela.model.user.ExecuteCommandDTO
import ccsl.tramela.service.MqttImpl
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


data class CommandDTO (
    val command: String,
    val description: String
)

@RestController
@RequestMapping("/commands")
class CommandsController (
    private val mqttImpl: MqttImpl
) {
    val commands = mapOf(
        "reboot" to "Reinicia fechadura",
        "open" to "Abre fechadura"
    )

    @PreAuthorize("authenticated")
    @PostMapping("")
    fun executeCommand(@RequestBody executeCommandDTO: ExecuteCommandDTO): ResponseEntity<MessageDTO> {
        mqttImpl.publishCommand(executeCommandDTO.command, executeCommandDTO.recipient)
        return ResponseEntity.ok(MessageDTO("Command executed"))
    }

    @PreAuthorize("authenticated")
    @GetMapping("")
    fun getCommands(): ResponseEntity<MutableList<CommandDTO>> {
        val listOfCommands = mutableListOf<CommandDTO>()

        for ((command, description) in commands) {
            listOfCommands.add(CommandDTO(command, description))
        }

        return ResponseEntity.ok(listOfCommands)
    }
}
