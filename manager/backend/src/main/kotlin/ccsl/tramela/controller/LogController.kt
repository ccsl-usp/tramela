package ccsl.tramela.controller

import ccsl.tramela.model.admin.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/log")
class LogController (
    private val accessRepository: AccessRepository,
    private val systemRepository: SystemRepository
) {

    @PreAuthorize("authenticated")
    @GetMapping("/system")
    fun getLogSystem(@RequestBody getLogDTO: GetLogDTO): Page<System> {
        val pageable: Pageable = PageRequest.of(getLogDTO.page, getLogDTO.size)
        return systemRepository.findAll(pageable)
    }

    @PreAuthorize("authenticated")
    @GetMapping("/access")
    fun getAccessSystem(@RequestBody getLogDTO: GetLogDTO): Page<Access> {
        val pageable: Pageable = PageRequest.of(getLogDTO.page, getLogDTO.size)
        return accessRepository.findAll(pageable)
    }
}

