package ccsl.tramela.controller

import ccsl.tramela.model.MessageDTO
import ccsl.tramela.model.user.*
import ccsl.tramela.service.MqttImpl
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/group")
class GroupController (
    private val groupRepository: GroupRepository,
    private val userGroupRepository: UserGroupRepository,
    private val groupDoorRepository: GroupDoorRepository,
    private val mqttImpl: MqttImpl,
    private val doorRepository: DoorRepository,
    private val userRepository: UserRepository
) {
    @PreAuthorize("authenticated")
    @GetMapping
    fun getGroup(): ResponseEntity<MutableList<GroupDTO>> {
        val groups = groupRepository.findAll()

        val getGroups = mutableListOf<GroupDTO>()

        for (group in groups) {
            val groupDoor = groupDoorRepository.findByGroupId(group.id)
            var doors = mutableListOf<Int>()
            for (element in groupDoor) {
                doors.add(element.door.id)
            }

            getGroups.add(GroupDTO(group.id, group.name, doors))
        }
        return ResponseEntity.ok(getGroups)
    }

    @PreAuthorize("authenticated")
    @PostMapping("")
    fun createGroup(@RequestBody createGroupDTO: CreateGroupDTO): ResponseEntity<MessageDTO> {
        val group = Group(name = createGroupDTO.name)
        groupRepository.save(group)

        for (doorId in createGroupDTO.doors) {
            doorRepository.findById(doorId)?.let { GroupDoor(id = GroupDoorId(it.id, group.id), group = group, door = it) }
                ?.let { groupDoorRepository.save(it) }
        }

        mqttImpl.publishUserDb()

        return ResponseEntity.ok(MessageDTO("Group created"))
    }

    @PreAuthorize("authenticated")
    @PutMapping("")
    fun updateGroup(@RequestBody updateGroupDTO: UpdateGroupDTO): ResponseEntity<MessageDTO> {
        var group = groupRepository.findById(updateGroupDTO.id)

        if (group.isPresent) {
            for (groupDoor in groupDoorRepository.findByGroupId(group.get().id)) {
                if (groupDoor.door.id !in updateGroupDTO.doors)
                    groupDoorRepository.delete(groupDoor)
            }

            for (doorId in updateGroupDTO.doors) {
                val door = doorRepository.findById(doorId)
                if (door != null) {
                    groupDoorRepository.save(GroupDoor(id = GroupDoorId(doorId =  door.id, groupId = group.get().id), door = door, group = group.get()))
                }
            }

            var group_ = group.get()
            group_.name = updateGroupDTO.name

            groupRepository.save(group_)

            mqttImpl.publishUserDb()

            return ResponseEntity.ok(MessageDTO("Group updated"))
        }

        return ResponseEntity.ok(MessageDTO("Group not found"))
    }

    @PreAuthorize("authenticated")
    @DeleteMapping("")
    fun deleteGroup(@RequestBody deleteGroupDTO: DeleteGroupDTO): ResponseEntity<MessageDTO> {
        val group = groupRepository.findById(deleteGroupDTO.id)

        if (!group.isEmpty) {
            for (groupDoor in groupDoorRepository.findByGroupId(group.get().id)) {
                groupDoorRepository.delete(groupDoor)
            }

            for (userGroup in userGroupRepository.findByGroupId(group.get().id)) {
                userGroupRepository.delete(userGroup)
            }

            groupRepository.delete(group.get())

            mqttImpl.publishUserDb()

            return ResponseEntity.ok(MessageDTO("Group deleted"))
        }
        return ResponseEntity.ok(MessageDTO("Group not found"))
    }
}


