package ccsl.tramela.controller

import ccsl.tramela.model.MessageDTO
import ccsl.tramela.model.user.*
import ccsl.tramela.service.MqttImpl
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/user")
class UserController (
    private val userRepository: UserRepository,
    private val authRepository: AuthRepository,
    private val doorRepository: DoorRepository,
    private val mqttImpl: MqttImpl,
    private val userGroupRepository: UserGroupRepository,
    private val groupRepository: GroupRepository
){
    @PreAuthorize("authenticated")
    @PostMapping("")
    fun createUser(@RequestBody createUserDTO: CreateUserDTO): ResponseEntity<MessageDTO> {
        val newUser = User(id = createUserDTO.id, email = createUserDTO.email, name = createUserDTO.name)
        userRepository.save(newUser)

        for (doorId in createUserDTO.doors) {
            doorRepository.findById(doorId)?.let { Auth(AuthId(it.id, newUser.id), it, newUser) }?.let { authRepository.save(it) }
        }

        for (groupId in createUserDTO.groups) {
            groupRepository.findById(groupId)?.let { UserGroup(UserGroupId(newUser.id, it.get().id), newUser, it.get()) }?.let { userGroupRepository.save(it) }
        }

        mqttImpl.publishUserDb()

        return ResponseEntity.ok(MessageDTO("User created"))
    }

    @PreAuthorize("authenticated")
    @PutMapping("")
    fun updateUser(@RequestBody updateUserDTO: UpdateUserDTO): ResponseEntity<MessageDTO> {
        val user = userRepository.findById(updateUserDTO.id)

        if (user.isEmpty) throw UsernameNotFoundException("User not found")

        for (doorId in updateUserDTO.doors) {
            doorRepository.findById(doorId)?.let { Auth(AuthId(it.id, user.get().id), it, user.get()) }
                ?.let { authRepository.save(it) }
        }

        for (groupId in updateUserDTO.groups) {
            groupRepository.findById(groupId)?.let { UserGroup(UserGroupId(user.get().id, it.get().id), user.get(), it.get()) }?.let { userGroupRepository.save(it) }
        }

        val auths = authRepository.findByUserId(user.get().id)
        val groupUsers = userGroupRepository.findByUserId(user.get().id)

        for (auth in auths) {
            if (auth.door.id !in updateUserDTO.doors)
                authRepository.delete(auth)
        }

        for (groupUser in groupUsers) {
            if (groupUser.group.id !in updateUserDTO.groups)
                userGroupRepository.delete(groupUser)
        }

        var user_ = user.get()
        user_.name = updateUserDTO.name
        user_.email = updateUserDTO.email

        userRepository.save(user_)

        mqttImpl.publishUserDb()

        return ResponseEntity.ok(MessageDTO("User updated"))
    }

    @PreAuthorize("authenticated")
    @DeleteMapping("")
    fun deleteUser(@RequestBody deleteUserDTO: DeleteUserDTO): ResponseEntity<MessageDTO> {
        val user = userRepository.findById(deleteUserDTO.id)

        if (user.isEmpty) throw UsernameNotFoundException("User doesn't exist")

        println(userGroupRepository.findByUserId(user.get().id))
        for (userGroup in userGroupRepository.findByUserId(user.get().id)) {
            userGroupRepository.delete(userGroup)
        }

        println(authRepository.findByUserId(user.get().id))
        for (auth in authRepository.findByUserId(user.get().id)) {
            authRepository.delete(auth)
        }

        userRepository.delete(user.get())

        mqttImpl.publishUserDb()

        return ResponseEntity.ok(MessageDTO("User deleted"))
    }

    @PreAuthorize("authenticated")
    @GetMapping
    fun getUser(): ResponseEntity<MutableList<UserDTO>> {
        var usersDTO = mutableListOf<UserDTO>()

        val users = userRepository.findAll()

        for (user in users) {
            val auths = authRepository.findByUserId(user.id)
            val doors = mutableListOf<Int>()

            for (auth in auths) {
                doors.add(auth.door.id)
            }

            val userGroups = userGroupRepository.findByUserId(user.id)
            val groups = mutableListOf<Long>()

            for (userGroup in userGroups) {
                groups.add(userGroup.group.id)
            }

            usersDTO.add(UserDTO(id =  user.id, name =  user.name, email = user.email, doors = doors, groups = groups))
        }
        return ResponseEntity.ok(usersDTO)
    }

}
