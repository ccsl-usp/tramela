package ccsl.tramela.controller

import ccsl.tramela.model.*
import ccsl.tramela.model.user.*
import ccsl.tramela.model.user.CreateDoorLockDTO
import ccsl.tramela.model.user.DeleteDoorLockDTO
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/door")
class DoorController(
    private val doorRepository: DoorRepository,
    private val groupDoorRepository: GroupDoorRepository,
    private val authRepository: AuthRepository,
    private val groupRepository: GroupRepository
) {
    @PreAuthorize("authenticated")
    @GetMapping("")
    fun getDoorLocks(): ResponseEntity<MutableList<DoorDTO>> {

        val doors = doorRepository.findAll()

        val getDoors = mutableListOf<DoorDTO>()

        for (door in doors) {
            val groupDoor = groupDoorRepository.findByDoorId(door.id)
            var groups = mutableListOf<Long>()
            for (element in groupDoor) {
                groups.add(element.group.id)
            }

            getDoors.add(DoorDTO(door.id, door.location, groups))
        }


        return ResponseEntity.ok(getDoors)
    }

    @PreAuthorize("authenticated")
    @PostMapping("")
    fun createDoorLock(@RequestBody createDoorLockDTO: CreateDoorLockDTO): ResponseEntity<MessageDTO> {
        val newDoor = Door(location = createDoorLockDTO.location)
        doorRepository.save(newDoor)

        for (groupId in createDoorLockDTO.groups) {
            val group = groupRepository.findById(groupId)

            if (group.isEmpty) continue

            groupDoorRepository.save(GroupDoor(GroupDoorId(newDoor.id, group.get().id), door = newDoor, group = group.get()))
        }

        return ResponseEntity.ok(MessageDTO("Door with id ${newDoor.id} created successfully"))
    }


    @PreAuthorize("authenticated")
    @PutMapping("")
    fun updateDoorLock(@RequestBody updateDoorLockDTO: UpdateDoorLockDTO): ResponseEntity<MessageDTO> {
        val door = doorRepository.findById(updateDoorLockDTO.id) ?: throw Exception("Door not found")

        door.location = updateDoorLockDTO.location

        doorRepository.save(door)

        for (groupId in updateDoorLockDTO.groups) {
            val group = groupRepository.findById(groupId)

            if (group.isEmpty) continue

            groupDoorRepository.save(GroupDoor(GroupDoorId(door.id, group.get().id), door = door, group = group.get()))
        }

        return ResponseEntity.ok(MessageDTO("Door with id ${door.id} updated successfully"))
    }

    @PreAuthorize("authenticated")
    @DeleteMapping("")
    fun deleteDoorLock(@RequestBody deleteDoorLockDTO: DeleteDoorLockDTO): ResponseEntity<MessageDTO> {
        val existingDoorLock: Door = doorRepository.findById(deleteDoorLockDTO.id)
            ?: throw NullPointerException("Door doesn't exist")

        for (groupDoor in groupDoorRepository.findByDoorId(existingDoorLock.id)) {
            groupDoorRepository.delete(groupDoor)
        }

        for (auth in authRepository.findByDoorId(existingDoorLock.id)) {
            authRepository.delete(auth)
        }

        doorRepository.delete(existingDoorLock)

        return ResponseEntity.ok(MessageDTO("Door ${deleteDoorLockDTO.id}  removed successfully"))
    }


}
