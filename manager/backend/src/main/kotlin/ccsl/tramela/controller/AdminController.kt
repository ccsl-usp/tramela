package ccsl.tramela.controller

import ccsl.tramela.config.JWTTokenProvider
import ccsl.tramela.model.*
import ccsl.tramela.model.admin.*
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/admin")
class AdminController (
    private val authenticationManager: AuthenticationManager,
    private val adminRepository: AdminRepository,
    private val jwtTokenProvider: JWTTokenProvider,
    private val passwordEncoder: PasswordEncoder
) {
    @PostMapping("/signin")
    @PreAuthorize("anonymous")
    fun signIn(@Valid @RequestBody loginUserDTO: LoginUserDTO): ResponseEntity<MessageDTO> {
        val email: String = loginUserDTO.email
        val password: String = loginUserDTO.password

        val authenticationToken = UsernamePasswordAuthenticationToken(email, password)
        var authentication = authenticationManager.authenticate(authenticationToken)

        val admin = adminRepository.findByEmail(authentication.name)
        val token = admin?.let { jwtTokenProvider.generateJwtToken(it) }

        return ResponseEntity(MessageDTO(token!!),  HttpStatus.OK)
    }

    @PostMapping("/signup")
    @PreAuthorize("anonymous")
    fun signup(@Valid @RequestBody admin: CreateAdminDTO): ResponseEntity<MessageDTO> {
        val email: String = admin.email
        val password: String = admin.password
        val encodedPassword: String = passwordEncoder.encode(password)
        val newAdmin = Admin(email = email, password = encodedPassword, name = admin.name, imageUri = admin.imageUri)

        if (adminRepository.existsByEmail(email)) {
            return ResponseEntity(MessageDTO("Email already exists"), HttpStatus.BAD_REQUEST)
        }

        adminRepository.save(newAdmin)
        return ResponseEntity(MessageDTO("User registered successfully"), HttpStatus.CREATED)
    }


    @GetMapping("")
    @PreAuthorize("authenticated")
    fun getAdminInfo(): ResponseEntity<AdminInfoDTO> {
        val currentAdminAuthenticated = SecurityContextHolder.getContext().authentication
        val admin = adminRepository.findByEmail(currentAdminAuthenticated.name)

        if (admin != null) {
            return ResponseEntity(AdminInfoDTO(name = admin.name, email = admin.email, imageUri = admin.imageUri, id = admin.id),  HttpStatus.OK)
        }
        throw UsernameNotFoundException("Admin not found, contact support")
    }
}
