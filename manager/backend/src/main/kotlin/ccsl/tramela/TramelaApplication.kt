package ccsl.tramela

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.core.task.TaskExecutor
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor

@SpringBootApplication
@EnableAsync
@EnableScheduling
class TramelaApplication {

    @Autowired
    lateinit var messageListener: Runnable

    @Profile("!test")
    @Bean
    fun taskExecutor(): TaskExecutor {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = 10 // Configure the number of threads
        executor.initialize()
        return executor
    }

    @Bean
    fun startListening(@Qualifier("taskExecutor") executor: TaskExecutor): CommandLineRunner {
        return CommandLineRunner { executor.execute(messageListener) }
    }
}


fun main(args: Array<String>) {
    runApplication<TramelaApplication>(*args)
}
