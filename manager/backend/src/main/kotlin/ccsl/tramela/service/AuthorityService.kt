package ccsl.tramela.service

import jakarta.annotation.PostConstruct
import org.springframework.stereotype.Service

@Service
class AuthorityService {

    private val commandPermissions: MutableMap<String, String> = HashMap()

    @PostConstruct
    fun initializeCommands() {
        commandPermissions["reboot"] = "something:idk"
        // ...
    }

    fun getCommandPermissions(): Map<String, String> {
        return commandPermissions
    }


    private val initialUserAuthorities = setOf(
        ""
    )

    private val availableAuthorities = setOf(
        ""
    ) + initialUserAuthorities

    fun adminAuthorities(): Set<String> {
        return availableAuthorities
    }


}
