package ccsl.tramela.service

import io.klogging.NoCoLogging
import jakarta.annotation.PostConstruct
import org.eclipse.paho.client.mqttv3.*
import org.springframework.context.annotation.Profile
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import java.nio.file.Files
import java.nio.file.Paths


@Component
@Profile("!test")
class MqttImpl(
    private val mqttConfig: MqttConfig
) : MqttCallback, NoCoLogging {

    private var mqttClient: MqttClient = mqttConfig.mqttClient ?: throw IllegalArgumentException("MQTT client cannot be null")
    private var qos = mqttConfig.qos

    @PostConstruct
    fun init() {
        mqttClient.setCallback(this)
    }

    /**
     * Given a command and a recipient (to whom the command is intended), the function publishes
     * the command in the correct topic.
     */
     fun publishCommand(command: String, recipient: String) {
        try {
            val message = "$recipient/$command"
            val mqttMessage = MqttMessage(message.toByteArray())
            mqttClient.publish("/topic/commands", mqttMessage)
            logger.info("Published command '$command' to recipient '$recipient'")
        } catch (e: MqttException) {
            logger.error("Error publishing command '$command' to recipient '$recipient', error: $e")
        }
    }

    companion object {
        fun getDbContent(): MqttMessage {
            val resourcePath = "db/data/user.db"
            val resource = ClassPathResource(resourcePath)
            val fileContent = Files.readAllBytes(Paths.get(resource.uri))
            return MqttMessage(fileContent)
        }
    }
    fun publishUserDb(): Boolean {
        try {
            mqttClient.publish("topic/database", getDbContent())

            mqttClient.disconnect()
            mqttClient.close()
        } catch (e: Exception) {
            logger.error("Exception caught when sending userdb via mqtt: ${e.message} ${e.cause}")
        }
        return true
    }

    fun subscribeMessage(topic: String) {
        try {
            mqttClient.subscribe(topic, qos)
            logger.info("Subscribed to topic '$topic'")
        } catch (e: MqttException) {
            logger.error("Error subscribing to topic '$topic'", e)
        }
    }

    fun disconnect() {
        try {
            mqttClient.disconnect()
            logger.info("MQTT client disconnected")
        } catch (e: MqttException) {
            logger.error("Error disconnecting MQTT client $e")
        }
    }
    @Throws(Exception::class)
    override fun messageArrived(mqttTopic: String, mqttMessage: MqttMessage) {
        logger.info("Received message on topic '$mqttTopic' with content '${String(mqttMessage.payload)}'")

        for (msg in mqttMessage.payload.toString().split("\\0")) {
            processMessage(msg)
        }
    }

    fun processMessage(msg: String) {
        if (msg.isEmpty()) return

        var msgFields = msg.split(" ")
        lateinit var timestamp: String
        var doorId = -1
        lateinit var msgtype: String
        var isAccess = false
        var isBoot = false

        try {

            timestamp = msgFields[0]
            doorId = msgFields[1][1].digitToInt()
            msgtype = msgFields[2].substring(1, msgFields[2].length - 1)
            isAccess = msgtype.contains("ACCESS")
            isBoot = msgtype.contains("BOOT")

        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (isBoot) {
            val start = msgtype.indexOf("#")
            val end = msgtype.indexOf(")", start)
            val bootCount = msgtype.substring(start + 1, end).toInt()
        }

        if (isAccess) {
            val readerID = if (msgFields[4].toString().endsWith("external")) 1 else 2
            val cardID = msgFields[6].toInt()
            val authOK = if (msgFields[7] == "authorized") 1 else 0

            // save stuff
        } else {
            val logText = msgFields.toString().substring(3, msgFields.size)
            // save stuff
        }
    }

    override fun connectionLost(cause: Throwable) {
        logger.error("Connection lost $cause")
        mqttConfig.config()  // Reconnect logic can be more sophisticated based on the application's needs
    }

    override fun deliveryComplete(token: IMqttDeliveryToken) {
        // Optional: Log or handle completed delivery
        logger.info("Delivery complete for message with id ${token.messageId}")
    }
}
