package ccsl.tramela.service

import ccsl.tramela.model.admin.Admin
import ccsl.tramela.model.admin.AdminRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException

class UserDetailsServiceImpl: UserDetailsService {

    @Autowired
    private lateinit var adminRepository: AdminRepository

    /**
     * This function is responsible for finding and returning the correspondent User object when someone authenticates.
     * It's used internally by Spring Security.
     */
    override fun loadUserByUsername(email: String?): UserDetails {
        val admin: Admin = adminRepository.findByEmail(email.toString())
                         ?: throw UsernameNotFoundException("User not found with email: $email")
        return CustomAdminDetails(
            admin.email,
            admin.password!!
        )
    }
}
