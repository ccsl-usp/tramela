package ccsl.tramela.service

import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import java.io.FileInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.security.KeyFactory
import java.security.KeyStore
import java.security.PrivateKey
import java.security.cert.Certificate
import java.security.cert.CertificateFactory
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory

@Component
@Profile("!test")
class MqttConfig (
    @Value("\${mqtt.ssl.ca-store}")
    private val caPath: String,
    @Value("\${mqtt.ssl.cert-store}")
    private val certPath: String,
    @Value("\${mqtt.ssl.key-store}")
    private val keyPath: String
) {
    protected val broker: String = "127.0.0.1"
    val qos: Int = 1
    protected var port: Int = 8883
    protected val TCP: String = "tcp://"
    protected val SSL: String = "ssl://"

    private var brokerUrl: String? = ""
    private val clientId = "TramelaManager"

    var mqttClient: MqttClient? = null
    private var connectionOptions: MqttConnectOptions? = null
    private var persistence: MemoryPersistence? = null

    lateinit var sslContext: SSLContext
    lateinit var socketFactory: SSLSocketFactory
    lateinit var tmf: TrustManagerFactory

    init {
        this.config()
    }

    fun pemToPKCS8(pemKey: String): PKCS8EncodedKeySpec {
        val lines = pemKey.lines().filter { it.isNotBlank() && !it.startsWith("-----") }
        val base64EncodedKey = lines.joinToString("")

        val keyBytes = Base64.getDecoder().decode(base64EncodedKey)
        return PKCS8EncodedKeySpec(keyBytes)
    }

    fun connectionLost(cause: Throwable) {
        this.config()
    }

    fun config() {
        this.configTls()
        this.brokerUrl = this.SSL + this.broker + ":" + this.port

        this.persistence = MemoryPersistence()
        this.connectionOptions = MqttConnectOptions()
        this.mqttClient = MqttClient(brokerUrl, clientId, persistence)

        connectionOptions!!.isCleanSession = true

        connectionOptions!!.socketFactory = socketFactory
        mqttClient!!.connect(this.connectionOptions)
    }

     fun configTls() {
         // Create CertificateFactory
         val cf = CertificateFactory.getInstance("X.509")

         // Generate certificates from InputStreams

         val caCertificate: Collection<Certificate?> = cf.generateCertificates(FileInputStream(caPath))
         val certCertificate: Collection<Certificate?> = cf.generateCertificates(FileInputStream(certPath))

         val pemKeyData = String(Files.readAllBytes(Paths.get(keyPath)), StandardCharsets.UTF_8)
         val spec = pemToPKCS8(pemKeyData)
         println(spec)

         val kf = KeyFactory.getInstance("RSA")
         val privateKey: PrivateKey = kf.generatePrivate(spec)

         // Store the certificates in the KeyStore
         val ks = KeyStore.getInstance(KeyStore.getDefaultType())
         ks.load(null, null)
         ks.setCertificateEntry("ca", caCertificate.first())
         ks.setCertificateEntry("server", certCertificate.first())
         ks.setKeyEntry("key", privateKey, null, certCertificate.toTypedArray())

         // Create the KeyManager
         val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
         kmf.init(ks, null)

         // Create the TrustManager
         val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
         tmf.init(ks)

         // Initialize the SSLContext
         val sslContext = SSLContext.getInstance("TLS")
         sslContext.init(kmf.keyManagers, tmf.trustManagers, null)

         // Set the configured SSLContext and socket factory
         this.sslContext = sslContext
         this.socketFactory = sslContext.socketFactory
     }
}

