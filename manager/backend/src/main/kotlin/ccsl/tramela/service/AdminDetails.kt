package ccsl.tramela.service

import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class CustomAdminDetails(
    private val email: String,
    private val password: String
) : UserDetails {
    override fun getAuthorities() = emptyList<SimpleGrantedAuthority>()

    override fun getPassword() = password

    override fun getUsername() = email

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true
}
