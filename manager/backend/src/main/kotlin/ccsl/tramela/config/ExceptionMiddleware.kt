package ccsl.tramela.config

import ccsl.tramela.model.ErrorMessageDTO
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionMiddleware {

    @ExceptionHandler(AuthenticationException::class)
    fun handleMethodArgumentNotValid(ex: AuthenticationException): ResponseEntity<Any> {
        val errorMessage = ErrorMessageDTO(
            location = "AuthController",
            message = "Login failed",
            action = ex.message.toString()
        )
        return ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST)
    }

}
