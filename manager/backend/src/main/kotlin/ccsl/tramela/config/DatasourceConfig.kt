package ccsl.tramela.config

import jakarta.persistence.EntityManagerFactory
import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.*
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.PlatformTransactionManager
import javax.sql.DataSource

@Profile("!test")
@Configuration
class DatasourceConfig {
    @Bean
    fun entityManagerFactoryBuilder(): EntityManagerFactoryBuilder {
        return EntityManagerFactoryBuilder(HibernateJpaVendorAdapter(), HashMap<String, Any>(), null)
    }
//}
}

@Configuration
@EnableJpaRepositories(
    basePackages = ["ccsl.tramela.model.admin"],
    entityManagerFactoryRef = "adminEntityManagerFactory",
    transactionManagerRef = "adminTransactionManager"
)
@Profile("!test") // Exclude this configuration from the "test" profile
class AdminDBConfig {
    @Bean(name = ["adminDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.admin")
    fun adminDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Bean(name = ["adminEntityManagerFactory"])
    fun entityManagerAdmin(
        builder: EntityManagerFactoryBuilder,
    ): LocalContainerEntityManagerFactoryBean {
        val properties = HashMap<String, Any>()
        properties["hibernate.dialect"] = "org.hibernate.community.dialect.SQLiteDialect"

        return builder
            .dataSource(adminDataSource())
            .packages("ccsl.tramela.model.admin")
            .persistenceUnit("adminDB")
            .properties(properties)
            .build()
    }

    @Primary
    @Bean
    fun adminTransactionManager(
        @Qualifier("adminEntityManagerFactory") adminEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(adminEntityManagerFactory)
    }

    @Bean(name = ["flywayAdmin"])
    @DependsOn("adminDataSource")
    fun flywayAdmin(
        @Value("\${spring.datasource.admin.migrations}") adminSchema: String
    ): Flyway {
        val flyway = Flyway.configure()
            .dataSource(adminDataSource())
            .locations("classpath:db/migrations/admin")
            .load()

        flyway.migrate()

        return flyway
    }
}


// -----------------------------------------------------------------------------------------------------
@Profile("!test") // Exclude this configuration from the "test" profile
@Configuration
@EnableJpaRepositories(
    basePackages = ["ccsl.tramela.model.user"],
    entityManagerFactoryRef = "userEntityManagerFactory",
    transactionManagerRef = "userTransactionManager"
)
class UserDBConfig {
    @Bean(name = ["userDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.user")
    fun userDataSource(): DataSource {
        return DataSourceBuilder.create()
            .build()
    }

    @Bean(name = ["userEntityManagerFactory"])
    fun entityManagerUser(
        builder: EntityManagerFactoryBuilder,
        @Qualifier("userDataSource") ds: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        val properties = HashMap<String, Any>()
        properties["hibernate.dialect"] = "org.hibernate.community.dialect.SQLiteDialect"


        return builder
            .dataSource(ds)
            .packages("ccsl.tramela.model.user")
            .persistenceUnit("userDB")
            .properties(properties)
            .build()
    }

    @Primary
    @Bean
    fun userTransactionManager(
        @Qualifier("userEntityManagerFactory") userEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(userEntityManagerFactory)
    }


    @Bean(name = ["flywayUser"])
    @DependsOn("userDataSource")
    fun flywayUser(
        @Qualifier("userDataSource") dataSource: DataSource,
        @Value("\${spring.datasource.user.migrations}") userSchema: String
    ): Flyway {
        val flyway = Flyway.configure()
            .dataSource(dataSource)
            .locations("classpath:db/migrations/user")
            .load()

        flyway.migrate()

        return flyway
    }
}
