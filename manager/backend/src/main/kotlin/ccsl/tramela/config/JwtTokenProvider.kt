package ccsl.tramela.config

import ccsl.tramela.model.admin.Admin
import jakarta.servlet.http.HttpServletRequest
import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jwk.RsaJsonWebKey
import org.jose4j.jwk.RsaJwkGenerator
import org.jose4j.jws.AlgorithmIdentifiers
import org.jose4j.jws.JsonWebSignature
import org.jose4j.jwt.JwtClaims
import org.jose4j.jwt.consumer.InvalidJwtException
import org.jose4j.jwt.consumer.JwtConsumer
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component


@Suppress("UNCHECKED_CAST")
@Component
class JWTTokenProvider {
    private val issuer = "Patagon"
    private val expirationTime = 60*24*14 // base is minutes: 60 minutes * 24 hours * 14 days = 2 weeks

    final var senderJwk: RsaJsonWebKey = RsaJwkGenerator.generateJwk(2048)

    private final var jwsAlgConstraints: AlgorithmConstraints = AlgorithmConstraints(
        AlgorithmConstraints.ConstraintType.PERMIT,
        AlgorithmIdentifiers.RSA_USING_SHA512
    )

    var jwtConsumer: JwtConsumer = JwtConsumerBuilder()
        .setRequireExpirationTime() // the JWT must have an expiration time
        .setMaxFutureValidityInMinutes(expirationTime) // but the  expiration time can't be too crazy TODO
        .setRequireSubject() // the JWT must have a subject claim
        .setExpectedIssuer(issuer) // whom the JWT needs to have been issued by
        .setExpectedAudience("Front-end") // to whom the JWT is intended for
        .setVerificationKey(senderJwk.publicKey) // verify the signature with the sender's public key
        .setJwsAlgorithmConstraints(jwsAlgConstraints) // limits the acceptable signature algorithm(s)
        .build() // create the JwtConsumer instance

    fun generateJwtToken(admin: Admin): String {
        val claims = getClaimsFromUser(admin)
        val jws = JsonWebSignature()

        // The payload of the JWS is JSON content of the JWT Claims
        jws.payload = claims.toJson()

        // The JWT is signed using the sender's private key
        jws.key = senderJwk.privateKey

        // Set the Key ID (kid) header because it's just the polite thing to do.
        // We only have one signing key in this example but a using a Key ID helps
        // facilitate a smooth key rollover process
        // jws.keyIdHeaderValue = senderJwk.keyId;

        // Set the signature algorithm on the JWT/JWS that will integrity protect the claims
        jws.algorithmHeaderValue = AlgorithmIdentifiers.RSA_USING_SHA512

        // Sign the JWS and produce the compact serialization, which will be the inner JWT/JWS
        // representation, which is a string consisting of three dot ('.') separated
        // base64url-encoded parts in the form Header.Payload.Signature
        val jwt = jws.compactSerialization

        return jwt
    }

    fun getAuthentication(
        token: String,
        authorities: List<GrantedAuthority>?,
        request: HttpServletRequest?
    ): Authentication {
        val jwtClaims = jwtConsumer.processToClaims(token)
        val email: String = jwtClaims.getClaimValue("email") as String
        val authToken = UsernamePasswordAuthenticationToken(email, null, null)
        authToken.details = WebAuthenticationDetailsSource().buildDetails(request)
        return authToken
    }


    fun isTokenValid(token: String): Boolean {
        try {
            jwtConsumer.processToClaims(token)
            return true
        } catch (e: InvalidJwtException) {
            // InvalidJwtException will be thrown, if the JWT failed processing or validation in any way.
            // Hopefully with meaningful explanations(s) about what went wrong.
            return false
        }
    }

    private fun getClaimsFromUser(admin: Admin): JwtClaims {
        val expirationTimeF: Float = expirationTime.toFloat()
        val claims = JwtClaims()
        claims.issuer = issuer // who creates the token and signs it
        claims.setAudience("Front-end") // to whom the token is intended to be sent
        claims.setExpirationTimeMinutesInTheFuture(expirationTimeF) // time when the token will expire (10 minutes from now)
        claims.setGeneratedJwtId() // a unique identifier for the token
        claims.setIssuedAtToNow() // when the token was issued/created (now)
//        claims.setNotBeforeMinutesInThePast(2f) // time before which the token is not yet valid (2 minutes ago) TODO
        claims.subject = "User info" // the subject/principal is whom the token is about
        claims.setClaim("email", admin.email)
        claims.setClaim("name", admin.name)
        claims.setClaim("imageUri", admin.imageUri)

        return claims
    }
}
