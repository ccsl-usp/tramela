package ccsl.tramela.config

import ccsl.tramela.service.MqttImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@Profile("!test")
class MqttMessageListener: Runnable {
    @Autowired
    lateinit var subscriber: MqttImpl

    override fun run() {
        subscriber.subscribeMessage("/topic/commands")
    }
}
