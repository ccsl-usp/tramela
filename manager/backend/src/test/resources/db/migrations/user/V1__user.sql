CREATE TABLE "auth" (
    userID    VARCHAR(255)    NOT NULL,
    doorID    VARCHAR(255)    NOT NULL,
    FOREIGN KEY (userID) REFERENCES "users"(id),
    FOREIGN KEY (doorID) REFERENCES "doors"(id)
);

CREATE TABLE "doors" (
    id              INTEGER         NOT NULL,
    location        VARCHAR(255)    NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE "users" (
    id    VARCHAR(255)    NOT NULL,
    name  VARCHAR(255)    NOT NULL,
    PRIMARY KEY (id)
);

-- Table to store groups
CREATE TABLE "groups" (
    id    SERIAL PRIMARY KEY,
    name  VARCHAR(255) NOT NULL UNIQUE
);

-- Table to associate users with groups
CREATE TABLE "user_groups" (
    userID    VARCHAR(255) NOT NULL,
    groupID   INTEGER      NOT NULL,
    FOREIGN KEY (userID) REFERENCES "users"(id),
    FOREIGN KEY (groupID) REFERENCES "groups"(id)
);

-- Table to associate groups with doors
CREATE TABLE "group_doors" (
    groupID   INTEGER      NOT NULL,
    doorID    INTEGER      NOT NULL,
    FOREIGN KEY (groupID) REFERENCES "groups"(id),
    FOREIGN KEY (doorID) REFERENCES "doors"(id)
);

CREATE index useridx on "auth"(userID, doorID);
