CREATE TABLE "admin" (
    id              UUID         NOT NULL,
    email           VARCHAR(254) NOT NULL UNIQUE,
    password        VARCHAR(60),
    name            VARCHAR(255) NOT NULL,
    imageUri        VARCHAR(255),

    PRIMARY KEY (id)
);

CREATE TABLE "access" (
    bootcount     INT,
    time          VARCHAR(40),
    door          INT,
    reader        INT,
    authorization INT,
    card          INT,
    UNIQUE (time, door, bootcount, reader, card, authorization)
);

CREATE TABLE "system" (
    bootcount INT,
    time      VARCHAR(40),
    door      INT,
    message   VARCHAR(1024),
    UNIQUE (time, bootcount, door, message)
);

