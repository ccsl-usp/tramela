package ccsl.tramela

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest

@SpringBootApplication
@SpringBootTest
class AuthenticationApplicationTests {

    @MockkBean
    lateinit var messageListener: Runnable

    @BeforeEach
    fun setup() {
        every { messageListener.run() } returns Unit
    }
}
