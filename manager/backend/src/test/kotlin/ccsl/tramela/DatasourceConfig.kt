package ccsl.tramela.config

import jakarta.persistence.EntityManagerFactory
import org.flywaydb.core.Flyway
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder
import org.springframework.context.annotation.*
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.test.context.TestPropertySource
import org.springframework.transaction.PlatformTransactionManager
import javax.sql.DataSource


@Configuration
@Profile("test")
class TestDBConfig {
    @Bean
    fun entityManagerFactoryBuilder(): EntityManagerFactoryBuilder {
        return EntityManagerFactoryBuilder(HibernateJpaVendorAdapter(), HashMap<String, Any>(), null)
    }
}

@Configuration
@TestPropertySource(locations=["classpath:test.properties"])
@Profile("test")
@EnableJpaRepositories(
    basePackages = ["ccsl.tramela.model.admin"],
    entityManagerFactoryRef = "testAdminEntityManagerFactory",
    transactionManagerRef = "testAdminTransactionManager"
)
class TestAdminDBConfig {
    @Bean(name = ["testAdminDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.admin")
    fun adminDataSource(): DataSource {
        return DataSourceBuilder.create().build()
    }

    @Bean(name = ["testAdminEntityManagerFactory"])
    fun testAuthEntityManagerFactory(
        builder: EntityManagerFactoryBuilder,
    ): LocalContainerEntityManagerFactoryBean {
        val properties = HashMap<String, Any>()
        properties["hibernate.dialect"] = "org.hibernate.community.dialect.SQLiteDialect"

        return builder
            .dataSource(adminDataSource())
            .packages("ccsl.tramela.model.admin")
            .persistenceUnit("adminDB")
            .properties(properties)
            .build()
    }

    @Primary
    @Bean
    fun testAuthTransactionManager(
        @Qualifier("testAdminEntityManagerFactory") adminEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(adminEntityManagerFactory)
    }

    @Bean(name = ["testFlywayAdmin"])
    @DependsOn("testAdminDataSource")
    fun flywayAdmin(
        @Value("\${spring.datasource.admin.migrations}") adminSchema: String
    ): Flyway {
        val flyway = Flyway.configure()
            .dataSource(adminDataSource())
            .locations("classpath:db/migrations/admin")
            .load()
        flyway.repair()

        flyway.migrate()

        return flyway
    }
}


// -----------------------------------------------------------------------------------------------------

@Configuration
@Profile("test")
@TestPropertySource(locations=["classpath:test.properties"])
@EnableJpaRepositories(
    basePackages = ["ccsl.tramela.model.user"],
    entityManagerFactoryRef = "testUserEntityManagerFactory",
    transactionManagerRef = "testUserTransactionManager"
)
class TestUserDBConfig {
    @Bean(name = ["testUserDataSource"])
    @ConfigurationProperties(prefix = "spring.datasource.user")
    fun testUserDataSource(): DataSource {
        return DataSourceBuilder.create()
            .build()
    }

    @Bean(name = ["testUserEntityManagerFactory"])
    fun testEntityManagerUser(
        builder: EntityManagerFactoryBuilder,
        @Qualifier("testUserDataSource") ds: DataSource
    ): LocalContainerEntityManagerFactoryBean {
        val properties = HashMap<String, Any>()
        properties["hibernate.dialect"] = "org.hibernate.community.dialect.SQLiteDialect"


        return builder
            .dataSource(ds)
            .packages("ccsl.tramela.model.user")
            .persistenceUnit("userDB")
            .properties(properties)
            .build()
    }

    @Primary
    @Bean
    fun testUserTransactionManager(
        @Qualifier("testUserEntityManagerFactory") userEntityManagerFactory: EntityManagerFactory
    ): PlatformTransactionManager {
        return JpaTransactionManager(userEntityManagerFactory)
    }


    @Bean(name = ["testFlywayUser"])
    @DependsOn("testUserDataSource")
    fun flywayUser(
        @Qualifier("testUserDataSource") dataSource: DataSource,
    ): Flyway {
        val flyway = Flyway.configure()
            .dataSource(dataSource)
            .locations("classpath:db/migrations/user")
            .load()
        flyway.repair()
        flyway.migrate()

        return flyway
    }
}
