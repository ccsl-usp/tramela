package ccsl.tramela.controller

import ccsl.tramela.TramelaApplication
import ccsl.tramela.service.MqttConfig
import ccsl.tramela.service.MqttImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource
import com.ninjasquad.springmockk.MockkBean
import org.junit.jupiter.api.Test
import org.sqlite.SQLiteDataSource
import java.io.File
import java.nio.file.Files
import java.sql.Connection
import java.sql.DatabaseMetaData

@ContextConfiguration(classes=[TramelaApplication::class])
@TestPropertySource(locations=["classpath:test.properties"])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
class CommandsControllerTest {

    @Autowired
    lateinit var commandController: CommandsController

    @MockkBean
    lateinit var mqttImpl: MqttImpl

    @MockkBean
    lateinit var mqttConfig: MqttConfig


    @Test
    fun `test get db command`() {
        val message = MqttImpl.getDbContent()

        val tempDir = Files.createTempDirectory("tempDB").toFile()
        val dbFile = File(tempDir, "database.db")
        dbFile.writeBytes(message.payload)

        val dataSource = SQLiteDataSource()
        dataSource.url = "jdbc:sqlite:${dbFile.absolutePath}"
        val connection: Connection = dataSource.connection
        val metaData: DatabaseMetaData = connection.metaData

        val tablesResultSet = metaData.getTables(null, null, null, arrayOf("TABLE"))

        val expectedTables = setOf("auth", "doors", "users")
        val actualTables = mutableListOf<String>()
        while (tablesResultSet.next()) {
            println(tablesResultSet.getString("TABLE_NAME"))
            actualTables.add(tablesResultSet.getString("TABLE_NAME"))
        }

        assert(actualTables.containsAll(expectedTables)) {
            "Expected tables ${expectedTables.joinToString()} not found in the database"
        }

        connection.close()
        tempDir.deleteRecursively()
    }
}
