//package ccsl.tramela.controller
//
//import ccsl.tramela.config.AuthorizationFilter
//import ccsl.tramela.config.MqttMessageListener
//import ccsl.tramela.config.SecurityConfig
//import ccsl.tramela.model.user.CreateUserDTO
//import ccsl.tramela.model.LoginUserDTO
//import ccsl.tramela.model.user.User
//import ccsl.tramela.model.UserRepository
//import com.fasterxml.jackson.databind.ObjectMapper
//import com.ninjasquad.springmockk.MockkBean
//import io.mockk.every
//import io.mockk.mockk
//import org.junit.jupiter.api.Assertions.assertEquals
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.assertThrows
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
//import org.springframework.context.annotation.ComponentScan
//import org.springframework.context.annotation.Import
//import org.springframework.http.HttpStatus
//import org.springframework.security.authentication.AuthenticationManager
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
//import org.springframework.security.crypto.password.PasswordEncoder
//import org.springframework.security.test.context.support.WithAnonymousUser
//import org.springframework.security.test.context.support.WithMockUser
//import org.springframework.test.context.ContextConfiguration
//import org.springframework.test.web.servlet.MockMvc
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
//import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
//
//@Import(UserController::class)
//@WebMvcTest(UserController::class)
//@WithMockUser
//@ContextConfiguration(classes = [SecurityConfig::class])
//@ComponentScan("package ccsl.tramela.config") // SecurityFilter
//class UserControllerTest {
//
//    @MockkBean
//    lateinit var userRepository: UserRepository
//
//    @MockkBean
//    lateinit var passwordEncoder: PasswordEncoder
//
//    @MockkBean
//    lateinit var authenticationManager: AuthenticationManager
//
//    @Autowired
//    lateinit var userController: UserController
//
//    @Autowired
//    lateinit var mockMvc: MockMvc
//
//    @Autowired
//    lateinit var authorizationFilter: AuthorizationFilter
//
//    @MockkBean
//    lateinit var mqttMessageListener: MqttMessageListener
//
//    @Test
//    @WithAnonymousUser
//    fun  `test invalid signup user`() {
//        val password = "Abcdefgh!123"
//        val email = "vinicius@patagon.work"
//        val createUserRequest = CreateUserDTO(email = email, name = "Vinicius Henrique",  password = password, mobile = null, imageUri = "test.com/image")
//
//        assertThrows<AccessDeniedException> { userController.signup(createUserRequest) }
//    }
//
//    @Test
//    @WithMockUser()
//    fun `test valid signup user`() {
//        val password = "Abcdefgh!123"
//        val email = "vinicius@patagon.work"
//        val createUserRequest = CreateUserDTO(email = email, name = "Vinicius Henrique",  password = password, mobile = null, imageUri = "test.com/image")
//
//        every { passwordEncoder.encode(password) } returns "fgkjdhfSJSh@5423ckjv"
//        every { userRepository.save(any()) } answers { User() }
//        every { userRepository.existsByEmail(email) } returns false
//
//        val result = userController.signup(createUserRequest)
//
//        assertEquals(HttpStatus.CREATED, result.statusCode)
//    }
//
//    @Test
//    @WithAnonymousUser
//    fun `test signin user`() {
//        val email = "vinicius@gmail.com"
//        val loginUserDTO = LoginUserDTO(email = email, password = "12345678ABc!")
//        val user = User(email = email)
//        val authentication: UsernamePasswordAuthenticationToken = mockk()
//
//        every { authenticationManager.authenticate(any()) } returns authentication
//        every { authentication.name } returns email
//        every { userRepository.findByEmail(email) } returns user
//
//        mockMvc.perform(post("/user/signin")
//            .contentType("application/json")
//            .content(ObjectMapper().writeValueAsString(loginUserDTO)))
//            .andExpect(status().isOk)
//    }
//
//    @Test
//    @WithMockUser(username = "vinicius@gmail.com")
//    fun `test signin already signed`() {
//        val loginUserDTO = LoginUserDTO(email = "vinicius@gmail.com", password = "12345678ABc!")
//        mockMvc.perform(post("/user/signin")
//            .contentType("application/json")
//            .content(ObjectMapper().writeValueAsString(loginUserDTO)))
//            .andExpect(status().isForbidden)
//    }
//}
