package ccsl.tramela.controller

import ccsl.tramela.TramelaApplication
import ccsl.tramela.model.user.CreateDoorLockDTO
import ccsl.tramela.model.user.DeleteDoorLockDTO
import ccsl.tramela.model.user.DoorRepository
import ccsl.tramela.service.MqttImpl
import com.ninjasquad.springmockk.MockkBean
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestPropertySource

@ContextConfiguration(classes=[TramelaApplication::class])
@ActiveProfiles("test")
@TestPropertySource(locations=["classpath:test.properties"])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SpringBootTest
@WithMockUser
class DoorControllerTest {

    @Autowired
    lateinit var doorRepository: DoorRepository

    @Autowired
    lateinit var doorController: DoorController

    @MockkBean
    lateinit var mqttImpl: MqttImpl

    @AfterEach
    fun clean() {
        doorRepository.deleteAll()
    }

    @Test
    fun `create door`() {
        val location = "quarto da sala"
        val id = 1
        doorController.createDoorLock(CreateDoorLockDTO(id, location))
        assert(doorRepository.existsById(id))
        assert(doorRepository.findById(id)!!.location == location)
    }


    @Test
    fun `delete door`() {
        val location = "quarto da sala"
        val id = 1
        doorController.createDoorLock(CreateDoorLockDTO(id, location))
        assert(doorRepository.existsById(id))
        assert(doorRepository.findById(id)!!.location == location)

        doorController.deleteDoorLock(DeleteDoorLockDTO(id))
        assert(!doorRepository.existsById(id))
    }
}
