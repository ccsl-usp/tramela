export const signInPage = "/auth/signin"

export const dashboardPage = "/dashboard"
export const doorPage = (doorId: string) => `${dashboardPage}/${doorId}`
export const doorsPage = "/doors"
export const groupsPage = "/groups"
export const usersPage = "/users"
export const settingsPage = "/settings"

export const defaultAuthenticatedPage = dashboardPage
