import { KeyboardEvent, RefObject } from "react"
import { type ClassValue, clsx } from "clsx"
import { twMerge } from "tailwind-merge"

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export const handleEnterAsClick = (
  e: KeyboardEvent<HTMLDivElement>,
  buttonRef: RefObject<HTMLButtonElement> | undefined
) => {
  if (e.key === "Enter" && !e.shiftKey) {
    e.preventDefault()
    buttonRef?.current?.click()
  }
}
