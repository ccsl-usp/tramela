import { z } from "zod"

export const doorSchema = z.object({
  id: z.number().int().positive(),
  name: z.string()
})

export type Door = z.infer<typeof doorSchema>
