import { z } from "zod"

export const groupSchema = z.object({
  id: z.number().int().positive(),
  name: z.string(),
  doors: z.array(z.number().int().positive())
})

export type Group = z.infer<typeof groupSchema>
