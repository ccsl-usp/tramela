import { z } from "zod"

export const commandSchema = z.object({
  command: z.string(),
  description: z.string()
})

export type Command = z.infer<typeof commandSchema>
