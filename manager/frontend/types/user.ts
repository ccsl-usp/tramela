import { z } from "zod"

export const userSchema = z.object({
  id: z.number().int().positive(),
  email: z.string().email(),
  name: z.string()
})

export type User = z.infer<typeof userSchema>
