"use client"

import { ReactNode, useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import { useUserProvider } from "@/contexts/user"
import { defaultAuthenticatedPage } from "@/routes"
import Loading from "../loading"

interface AuthLayoutProps {
  children: ReactNode
}

export default function AuthLayout({ children }: AuthLayoutProps) {
  const [loading, setLoading] = useState(true)

  const router = useRouter()
  const { user } = useUserProvider()

  useEffect(() => {
    if (user) {
      router.push(defaultAuthenticatedPage)
    } else {
      setLoading(false)
    }
  }, [router, user])

  if (loading) {
    return <Loading />
  }

  return children
}
