"use client"

import { useRef } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { z } from "zod"
import { handleEnterAsClick } from "@/lib/utils"
import { Button } from "@/components/ui/button"
import { FormInput } from "@/components/ui/custom/form-input"
import { Form, FormField } from "@/components/ui/form"

const signInSchema = z.object({
  email: z.string().email("Digite um email válido"),
  password: z.string().min(1, "Digite uma senha")
})

type SignIn = z.infer<typeof signInSchema>

export default function SignInPage() {
  const buttonRef = useRef<HTMLButtonElement>(null)

  const form = useForm<SignIn>({
    resolver: zodResolver(signInSchema),
    defaultValues: {
      email: "",
      password: ""
    }
  })

  const submit = async ({ email, password }: SignIn) => {
    // TODO
  }

  return (
    <div
      className="flex flex-col gap-8 w-screen h-screen justify-center items-center"
      onKeyDown={e => handleEnterAsClick(e, buttonRef)}
    >
      <h1 className="text-center">Tramela</h1>

      <Form {...form}>
        <form onSubmit={form.handleSubmit(submit)}>
          <div className="flex flex-col gap-6 w-96">
            <div className="flex flex-col gap-4">
              <FormField
                control={form.control}
                name="email"
                render={({ field }) => <FormInput label="Email" placeholder="email@exemplo.com" {...field} />}
              />

              <FormField
                control={form.control}
                name="password"
                render={({ field }) => <FormInput label="Senha" placeholder="••••••••" type="password" {...field} />}
              />
            </div>

            <Button className="w-full" type="submit" ref={buttonRef}>
              Entrar
            </Button>
          </div>
        </form>
      </Form>
    </div>
  )
}
