import { Roboto } from "next/font/google"
import { UserProvider } from "@/contexts/user"
import { ThemeProvider } from "next-themes"
import { Toaster } from "sonner"
import "./globals.css"

const roboto = Roboto({ weight: ["400", "700"], subsets: ["latin"] })

export default async function RootLayout({
  children
}: Readonly<{
  children: React.ReactNode
}>) {
  // TODO fetch
  const user = { id: 1, email: "pedro.mariano@usp.br", name: "Pedro" }
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]
  const commands = [
    { command: "open", description: "Abrir a porta" },
    { command: "reset", description: "Reiniciar a porta" }
  ]

  return (
    <html lang="pt-br">
      <body className={roboto.className}>
        <UserProvider user={user} doors={doors} commands={commands}>
          <ThemeProvider attribute="class" defaultTheme="dark">
            <Toaster theme="dark" position="top-right" richColors closeButton />
            {children}
          </ThemeProvider>
        </UserProvider>
      </body>
    </html>
  )
}
