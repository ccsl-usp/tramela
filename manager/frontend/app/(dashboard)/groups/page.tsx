"use client"

import { Suspense, useState } from "react"
import { CreateGroupDialog } from "@/components/ui/custom/create-group-dialog"
import { GroupsTableBody } from "@/components/ui/custom/groups-table-body"
import { Input } from "@/components/ui/input"
import { Table, TableHead, TableHeader, TableRow } from "@/components/ui/table"

export default function DoorsPage() {
  const [filter, setFilter] = useState("")

  return (
    <>
      <div className="space-y-8">
        <h2>Grupos</h2>

        <Input className="w-96" placeholder="Filtrar grupos" value={filter} onChange={e => setFilter(e.target.value)} />

        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-1/2">Nome do grupo</TableHead>
              <TableHead className="w-1/2">Portas</TableHead>
            </TableRow>
          </TableHeader>

          {/* TODO */}
          <Suspense fallback="Carregando grupos...">
            <GroupsTableBody filter={filter} />
          </Suspense>
        </Table>
      </div>

      <CreateGroupDialog />
    </>
  )
}
