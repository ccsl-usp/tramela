"use client"

import { useEffect, useState } from "react"
import { useRouter } from "next/navigation"
import { useUserProvider } from "@/contexts/user"
import { signInPage } from "@/routes"
import { Sidebar } from "@/components/sidebar"
import Loading from "../loading"

export default function DashboardLayout({ children }: { children: React.ReactNode }) {
  const [loading, setLoading] = useState(true)

  const router = useRouter()
  const { user } = useUserProvider()

  useEffect(() => {
    if (!user) {
      router.push(signInPage)
    } else {
      setLoading(false)
    }
  }, [router, user])

  if (loading) {
    return <Loading />
  }

  return (
    <div className="h-screen w-screen flex">
      <Sidebar className="w-72 h-full" />
      <div className="w-full overflow-auto px-4 py-8">{children}</div>
    </div>
  )
}
