import { Suspense } from "react"
import { redirect } from "next/navigation"
import { dashboardPage } from "@/routes"
import { CommandButton } from "@/components/ui/custom/command-button"
import { ScrollArea } from "@/components/ui/scroll-area"
import { DoorsSelector } from "@/components/doors-selector"
import { LastAccess } from "@/components/last-access"

interface DashboardDoorPageProps {
  params: {
    doorId: string
  }
}

export default async function DashboardDoorPage({ params: { doorId } }: DashboardDoorPageProps) {
  // TODO
  await new Promise(resolve => setTimeout(resolve, 1000))
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]
  const door = doors.find(door => door.id.toString() === doorId)
  const commands = [
    { command: "open", description: "Abrir a porta" },
    { command: "reset", description: "Reiniciar a porta" }
  ]

  if (!door) {
    redirect(dashboardPage)
  }

  return (
    <div className="space-y-8">
      <DoorsSelector doors={doors} door={door} />

      <div className="grid grid-cols-2 gap-8">
        <div className="flex flex-col gap-2">
          <h3>Comandos</h3>
          {commands.map(command => (
            <CommandButton key={command.command} command={command} />
          ))}
        </div>

        <div className="space-y-2">
          <h3>Logs</h3>
          <ScrollArea className="rounded border p-4 h-96">
            <Suspense fallback="Carregando acessos...">
              <LastAccess />
            </Suspense>
          </ScrollArea>
        </div>
      </div>
    </div>
  )
}
