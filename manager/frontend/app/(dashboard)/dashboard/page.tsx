import { DoorsSelector } from "@/components/doors-selector"

export default async function DashboardPage() {
  // TODO
  await new Promise(resolve => setTimeout(resolve, 1000))
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]

  return (
    <div className="space-y-8">
      <DoorsSelector doors={doors} />

      <h3 className="text-center opacity-50">Selecione uma porta</h3>
    </div>
  )
}
