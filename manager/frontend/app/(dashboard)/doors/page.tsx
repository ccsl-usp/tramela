"use client"

import { Suspense, useState } from "react"
import { CreateDoorDialog } from "@/components/ui/custom/create-door-dialog"
import { DoorsTableBody } from "@/components/ui/custom/doors-table-body"
import { Input } from "@/components/ui/input"
import { Table, TableHead, TableHeader, TableRow } from "@/components/ui/table"

export default function DoorsPage() {
  const [filter, setFilter] = useState("")

  return (
    <>
      <div className="space-y-8">
        <h2>Portas</h2>

        <Input className="w-96" placeholder="Filtrar portas" value={filter} onChange={e => setFilter(e.target.value)} />

        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-1/12">Id</TableHead>
              <TableHead>Nome da porta</TableHead>
            </TableRow>
          </TableHeader>

          {/* TODO */}
          <Suspense fallback="Carregando portas...">
            <DoorsTableBody filter={filter} />
          </Suspense>
        </Table>
      </div>

      <CreateDoorDialog />
    </>
  )
}
