"use client"

import { Suspense, useState } from "react"
import { UsersTableBody } from "@/components/ui/custom/users-table-body"
import { Input } from "@/components/ui/input"
import { Table, TableHead, TableHeader, TableRow } from "@/components/ui/table"
import { CreateUser } from "@/components/create-user"

export default function UsersPage() {
  const [filter, setFilter] = useState("")

  return (
    <>
      <div className="space-y-8">
        <h2>Usuários</h2>

        <Input
          className="w-96"
          placeholder="Filtrar usuários"
          value={filter}
          onChange={e => setFilter(e.target.value)}
        />

        <Table>
          <TableHeader>
            <TableRow>
              <TableHead className="w-1/2">Nome do usuário</TableHead>
              <TableHead className="w-1/2">Email</TableHead>
            </TableRow>
          </TableHeader>

          {/* TODO */}
          <Suspense fallback="Carregando usuários...">
            <UsersTableBody filter={filter} />
          </Suspense>
        </Table>
      </div>

      <Suspense>
        <CreateUser />
      </Suspense>
    </>
  )
}
