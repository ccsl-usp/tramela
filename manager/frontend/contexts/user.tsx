"use client"

import { FC, ReactNode, createContext, useContext, useState } from "react"
import { Command } from "@/types/command"
import { Door } from "@/types/door"
import { User } from "@/types/user"

interface UserContextProps {
  user?: User
  signIn: (user?: User) => void
  signOut: () => void

  doors: Door[]
  addDoor: (door: Door) => void

  commands: Command[]
}

const UserContext = createContext<UserContextProps>({} as UserContextProps)

interface UserProviderProps {
  user?: User
  doors: Door[]
  commands: Command[]
  children: ReactNode
}

export const UserProvider: FC<UserProviderProps> = ({ user: initialUser, doors: initialDoors, commands, children }) => {
  const [user, setUser] = useState<User | undefined>(initialUser)
  const [doors, setDoors] = useState<Door[]>(initialDoors)

  const signIn = (user?: User) => {
    setUser(user)
  }

  const signOut = () => {
    setUser(undefined)
  }

  const addDoor = (door: Door) => {
    setDoors(doors => [...doors, door])
  }

  return (
    <UserContext.Provider
      value={{
        user,
        signIn,
        signOut,
        doors,
        addDoor,
        commands
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

export const useUserProvider = () => useContext(UserContext)
