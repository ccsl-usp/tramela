import { FC } from "react"
import { dashboardPage, doorsPage, groupsPage, settingsPage, usersPage } from "@/routes"
import { cn } from "@/lib/utils"
import { SidebarLink } from "./sidebar-link"

interface SidebarProps {
  className?: string
}

export const Sidebar: FC<SidebarProps> = ({ className }) => {
  return (
    <div className={cn("bg-secondary flex flex-col gap-2", className)}>
      <h1 className="text-center mt-8">Tramela</h1>
      <div className="border-b-2 border-primary m-4" />

      <div className="flex flex-col gap-2">
        <SidebarLink href={dashboardPage}>Dashboard</SidebarLink>
        <SidebarLink href={doorsPage}>Portas</SidebarLink>
        <SidebarLink href={groupsPage}>Grupos</SidebarLink>
        <SidebarLink href={usersPage}>Usuários</SidebarLink>
        <SidebarLink href={settingsPage}>Configurações</SidebarLink>
      </div>
    </div>
  )
}
