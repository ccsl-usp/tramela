import { FC, HTMLInputTypeAttribute } from "react"
import { FormControl, FormItem, FormLabel, FormMessage } from "../form"
import { Input } from "../input"

interface FormInputProps {
  label: string
  placeholder?: string
  type?: HTMLInputTypeAttribute
}

export const FormInput: FC<FormInputProps> = ({ label, placeholder, type, ...props }) => {
  return (
    <FormItem className="flex flex-col w-full">
      <FormLabel className="text-lg">{label}</FormLabel>
      <FormControl>
        <Input className="max-w-md" placeholder={placeholder} type={type} {...props} />
      </FormControl>
      <FormMessage />
    </FormItem>
  )
}
