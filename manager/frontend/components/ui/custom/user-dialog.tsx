"use client"

import { FC, ReactNode, useEffect, useRef, useState } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { toast } from "sonner"
import { z } from "zod"
import { Door } from "@/types/door"
import { Group } from "@/types/group"
import { User } from "@/types/user"
import { handleEnterAsClick } from "@/lib/utils"
import { Button } from "../button"
import { Checkbox } from "../checkbox"
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "../dialog"
import { Form, FormControl, FormField, FormItem, FormLabel } from "../form"
import { DeleteButton } from "./delete-button"

const updateUserSchema = z.object({
  groups: z.array(z.number().int().positive()),
  doors: z.array(z.number().int().positive())
})

type UpdateUser = z.infer<typeof updateUserSchema>

interface UserDialogProps {
  user: User
  groups: Group[]
  doors: Door[]
  children: ReactNode
}

export const UserDialog: FC<UserDialogProps> = ({ user, groups, doors, children }) => {
  const [open, setOpen] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)

  const form = useForm<UpdateUser>({
    resolver: zodResolver(updateUserSchema),
    defaultValues: {
      groups: [],
      doors: []
    }
  })

  const submit = async (values: UpdateUser) => {
    setSubmitting(true)

    // TODO
    await new Promise(resolve => setTimeout(resolve, 1000))
    toast.success("Usuário atualizado com sucesso")

    setSubmitting(false)
  }

  useEffect(() => {
    // TODO fetch user doors
    const userGroups = [groups[0]]
    const userDoors = [doors[0]]

    form.setValue(
      "groups",
      userGroups.map(group => group.id)
    )
    form.setValue(
      "doors",
      userDoors.map(door => door.id)
    )
  }, [])

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>{children}</DialogTrigger>

      <DialogContent onKeyDown={e => handleEnterAsClick(e, buttonRef)}>
        <DialogHeader>
          <DialogTitle>Editar usuário</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(submit)} className="flex flex-col gap-4">
            <FormField
              control={form.control}
              name="groups"
              render={() => (
                <FormItem>
                  <FormLabel className="text-lg">Grupos</FormLabel>
                  {groups.map(group => (
                    <FormField
                      key={group.id}
                      control={form.control}
                      name="groups"
                      render={({ field }) => (
                        <FormItem className="flex items-center gap-2">
                          <FormControl>
                            <Checkbox
                              checked={field.value.includes(group.id)}
                              onCheckedChange={checked =>
                                field.onChange(
                                  checked
                                    ? [...field.value, group.id]
                                    : field.value.filter(groupId => groupId !== group.id)
                                )
                              }
                            />
                          </FormControl>
                          <FormLabel>{group.name}</FormLabel>
                        </FormItem>
                      )}
                    />
                  ))}
                </FormItem>
              )}
            />

            <FormField
              control={form.control}
              name="doors"
              render={() => (
                <FormItem>
                  <FormLabel className="text-lg">Portas</FormLabel>
                  {doors.map(door => (
                    <FormField
                      key={door.id}
                      control={form.control}
                      name="doors"
                      render={({ field }) => (
                        <FormItem className="flex items-center gap-2">
                          <FormControl>
                            <Checkbox
                              checked={field.value.includes(door.id)}
                              onCheckedChange={checked =>
                                field.onChange(
                                  checked ? [...field.value, door.id] : field.value.filter(doorId => doorId !== door.id)
                                )
                              }
                            />
                          </FormControl>
                          <FormLabel>{door.name}</FormLabel>
                        </FormItem>
                      )}
                    />
                  ))}
                </FormItem>
              )}
            />

            <DialogFooter>
              <div className="w-full flex justify-between">
                <DeleteButton message="Tem certeza que deseja apagar esse usuário?" />
                <div className="space-x-2">
                  <DialogClose>
                    <Button variant="ghost" type="reset" onClick={() => form.reset()} disabled={submitting}>
                      Cancelar
                    </Button>
                  </DialogClose>
                  <Button type="submit" ref={buttonRef} disabled={submitting}>
                    Salvar
                  </Button>
                </div>
              </div>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  )
}
