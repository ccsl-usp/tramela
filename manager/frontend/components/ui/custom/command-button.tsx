"use client"

import { FC, useState } from "react"
import { CirclePlay, Loader2 } from "lucide-react"
import { toast } from "sonner"
import { Command } from "@/types/command"
import { Button } from "../button"

interface CommandButtonProps {
  command: Command
}

export const CommandButton: FC<CommandButtonProps> = ({ command }) => {
  const [loading, setLoading] = useState(false)

  const handleClick = async () => {
    setLoading(true)
    // TODO
    await new Promise(resolve => setTimeout(resolve, 1000))
    toast.success(`Comando "${command.description}" executado com sucesso`)
    setLoading(false)
  }

  return (
    <Button
      key={command.command}
      className="w-full justify-between"
      variant="secondary"
      onClick={handleClick}
      disabled={loading}
    >
      {command.description} {loading ? <Loader2 className="animate-spin" size={18} /> : <CirclePlay size={18} />}
    </Button>
  )
}
