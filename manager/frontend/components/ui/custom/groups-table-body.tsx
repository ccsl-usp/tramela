import { FC } from "react"
import { TableBody, TableCell, TableRow } from "../table"
import { GroupDialog } from "./group-dialog"

interface GroupsTableBody {
  filter: string
}

export const GroupsTableBody: FC<GroupsTableBody> = async ({ filter }) => {
  // TODO pass filter to backend
  await new Promise(resolve => setTimeout(resolve, 1000))
  const groups = [
    { id: 1, name: "Alunos", doors: [2] },
    { id: 2, name: "Funcionários", doors: [1] }
  ]
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]

  return (
    <TableBody>
      {groups
        .sort((a, b) => a.name.localeCompare(b.name))
        .map(group => (
          <GroupDialog key={group.id} doors={doors}>
            <TableRow className="hover:cursor-pointer">
              <TableCell>{group.name}</TableCell>
              <TableCell>{group.doors}</TableCell>
            </TableRow>
          </GroupDialog>
        ))}
    </TableBody>
  )
}
