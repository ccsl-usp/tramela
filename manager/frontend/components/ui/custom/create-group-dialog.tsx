"use client"

import { FC, useRef, useState } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { Plus } from "lucide-react"
import { useForm } from "react-hook-form"
import { toast } from "sonner"
import { z } from "zod"
import { handleEnterAsClick } from "@/lib/utils"
import { Button } from "../button"
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "../dialog"
import { Form, FormField } from "../form"
import { FormInput } from "./form-input"

const createGroupSchema = z.object({
  name: z.string().min(1, "Digite um nome"),
  doors: z.array(z.number().int().positive()),
  users: z.array(z.number().int().positive())
})

type CreateGroup = z.infer<typeof createGroupSchema>

export const CreateGroupDialog: FC = () => {
  const [open, setOpen] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)

  const form = useForm<CreateGroup>({
    resolver: zodResolver(createGroupSchema),
    defaultValues: {
      name: "",
      doors: [],
      users: []
    }
  })

  const submit = async (values: CreateGroup) => {
    setSubmitting(true)

    // TODO
    await new Promise(resolve => setTimeout(resolve, 1000))
    // TODO update group in page
    toast.success("Grupo criado com sucesso")

    setOpen(false)
    setSubmitting(false)
    form.reset()
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger className="bg-primary rounded-full p-4 absolute bottom-8 right-8">
        <Plus />
      </DialogTrigger>

      <DialogContent onKeyDown={e => handleEnterAsClick(e, buttonRef)}>
        <DialogHeader>
          <DialogTitle>Adicionar usuário</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(submit)} className="flex flex-col gap-4">
            <FormField
              control={form.control}
              name="name"
              render={({ field }) => <FormInput label="Nome" {...field} />}
            />

            {/* TODO add doors and users selector */}

            <DialogFooter>
              <DialogClose>
                <Button variant="ghost" type="reset" onClick={() => form.reset()} disabled={submitting}>
                  Cancelar
                </Button>
              </DialogClose>
              <Button type="submit" ref={buttonRef} disabled={submitting}>
                Adicionar
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  )
}
