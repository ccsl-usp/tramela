import { FC } from "react"
import { Button } from "../button"
import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogFooter,
  DialogTitle,
  DialogDescription,
  DialogClose
} from "../dialog"

interface DeleteButtonProps {
  message: string
}

export const DeleteButton: FC<DeleteButtonProps> = ({ message }) => {
  return (
    <Dialog>
      <DialogTrigger>
        <Button variant="destructive">Apagar</Button>
      </DialogTrigger>

      <DialogContent>
        <DialogHeader>
          <DialogTitle>{message}</DialogTitle>
          <DialogDescription>Essa ação não poderá ser desfeita.</DialogDescription>
        </DialogHeader>

        <DialogFooter>
          <DialogClose>
            <Button variant="ghost">Cancelar</Button>
          </DialogClose>
          {/* TODO */}
          <Button variant="destructive">Apagar</Button>
        </DialogFooter>
      </DialogContent>
    </Dialog>
  )
}
