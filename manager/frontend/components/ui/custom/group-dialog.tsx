"use client"

import { FC, ReactNode, useEffect, useRef, useState } from "react"
import { zodResolver } from "@hookform/resolvers/zod"
import { useForm } from "react-hook-form"
import { toast } from "sonner"
import { z } from "zod"
import { Door } from "@/types/door"
import { handleEnterAsClick } from "@/lib/utils"
import { Button } from "../button"
import { Checkbox } from "../checkbox"
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "../dialog"
import { Form } from "../form"
import { FormControl, FormField, FormItem, FormLabel } from "../form"
import { DeleteButton } from "./delete-button"

const updateGroupSchema = z.object({
  doors: z.array(z.number().int().positive())
})

type UpdateUser = z.infer<typeof updateGroupSchema>

interface GroupDialogProps {
  doors: Door[]
  children: ReactNode
}

export const GroupDialog: FC<GroupDialogProps> = ({ doors, children }) => {
  const [open, setOpen] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)

  const form = useForm<UpdateUser>({
    resolver: zodResolver(updateGroupSchema),
    defaultValues: {
      doors: []
    }
  })

  const submit = async (values: UpdateUser) => {
    setSubmitting(true)

    // TODO
    await new Promise(resolve => setTimeout(resolve, 1000))
    toast.success("Grupo atualizado com sucesso")

    setSubmitting(false)
  }

  useEffect(() => {
    // TODO fetch group doors
    const groupDoors = [doors[0]]

    form.setValue(
      "doors",
      groupDoors.map(door => door.id)
    )
  }, [])

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger asChild>{children}</DialogTrigger>

      <DialogContent onKeyDown={e => handleEnterAsClick(e, buttonRef)}>
        <DialogHeader>
          <DialogTitle>Editar grupo</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(submit)} className="flex flex-col gap-4">
            <FormField
              control={form.control}
              name="doors"
              render={() => (
                <FormItem>
                  <FormLabel className="text-lg">Portas</FormLabel>
                  {doors.map(door => (
                    <FormField
                      key={door.id}
                      control={form.control}
                      name="doors"
                      render={({ field }) => (
                        <FormItem className="flex items-center gap-2">
                          <FormControl>
                            <Checkbox
                              checked={field.value.includes(door.id)}
                              onCheckedChange={checked =>
                                field.onChange(
                                  checked ? [...field.value, door.id] : field.value.filter(doorId => doorId !== door.id)
                                )
                              }
                            />
                          </FormControl>
                          <FormLabel>{door.name}</FormLabel>
                        </FormItem>
                      )}
                    />
                  ))}
                </FormItem>
              )}
            />

            <DialogFooter>
              <div className="w-full flex justify-between">
                <DeleteButton message="Tem certeza que deseja apagar esse grupo?" />
                <div className="space-x-2">
                  <DialogClose>
                    <Button variant="ghost" type="reset" onClick={() => form.reset()} disabled={submitting}>
                      Cancelar
                    </Button>
                  </DialogClose>
                  <Button type="submit" ref={buttonRef} disabled={submitting}>
                    Salvar
                  </Button>
                </div>
              </div>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  )
}
