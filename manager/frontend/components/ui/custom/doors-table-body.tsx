import { FC } from "react"
import { TableBody, TableCell, TableRow } from "../table"

interface DoorsTableBody {
  filter: string
}

export const DoorsTableBody: FC<DoorsTableBody> = async ({ filter }) => {
  // TODO pass filter to backend
  await new Promise(resolve => setTimeout(resolve, 1000))
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]

  return (
    <TableBody>
      {doors
        .sort((a, b) => a.name.localeCompare(b.name))
        .map(door => (
          <TableRow key={door.id}>
            <TableCell>{door.id}</TableCell>
            <TableCell>{door.name}</TableCell>
          </TableRow>
        ))}
    </TableBody>
  )
}
