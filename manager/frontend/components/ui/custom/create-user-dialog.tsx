"use client"

import { FC, useRef, useState } from "react"
import { useUserProvider } from "@/contexts/user"
import { zodResolver } from "@hookform/resolvers/zod"
import { Plus } from "lucide-react"
import { useForm } from "react-hook-form"
import { toast } from "sonner"
import { z } from "zod"
import { Door } from "@/types/door"
import { Group } from "@/types/group"
import { handleEnterAsClick } from "@/lib/utils"
import { Button } from "../button"
import { Dialog, DialogClose, DialogContent, DialogFooter, DialogHeader, DialogTitle, DialogTrigger } from "../dialog"
import { Form, FormField } from "../form"
import { FormInput } from "./form-input"

const createUserSchema = z.object({
  id: z.coerce.number().int().positive("id precisa ser um número positivo"),
  name: z.string().min(1, "Digite um nome"),
  email: z.string().email("Digite um email válido"),
  groups: z.array(z.number().int().positive()),
  doors: z.array(z.number().int().positive())
})

type CreateUser = z.infer<typeof createUserSchema>

interface CreateUserDialogProps {
  doors: Door[]
  groups: Group[]
}

export const CreateUserDialog: FC<CreateUserDialogProps> = ({ doors, groups }) => {
  const [open, setOpen] = useState(false)
  const [submitting, setSubmitting] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)

  const form = useForm<CreateUser>({
    resolver: zodResolver(createUserSchema),
    defaultValues: {
      id: 0,
      name: "",
      email: "",
      groups: [],
      doors: []
    }
  })

  const submit = async (values: CreateUser) => {
    setSubmitting(true)

    // TODO
    await new Promise(resolve => setTimeout(resolve, 1000))
    // TODO update user in page
    toast.success("Usuário adicionado com sucesso")

    setOpen(false)
    setSubmitting(false)
    form.reset()
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <DialogTrigger className="bg-primary rounded-full p-4 absolute bottom-8 right-8">
        <Plus />
      </DialogTrigger>

      <DialogContent onKeyDown={e => handleEnterAsClick(e, buttonRef)}>
        <DialogHeader>
          <DialogTitle>Adicionar usuário</DialogTitle>
        </DialogHeader>

        <Form {...form}>
          <form onSubmit={form.handleSubmit(submit)} className="flex flex-col gap-4">
            <FormField
              control={form.control}
              name="id"
              render={({ field }) => <FormInput label="Id" type="number" {...field} />}
            />

            <FormField
              control={form.control}
              name="name"
              render={({ field }) => <FormInput label="Nome" {...field} />}
            />

            <FormField
              control={form.control}
              name="email"
              render={({ field }) => <FormInput label="Email" {...field} />}
            />

            {/* TODO add groups and doors selector */}

            <DialogFooter>
              <DialogClose>
                <Button variant="ghost" type="reset" onClick={() => form.reset()} disabled={submitting}>
                  Cancelar
                </Button>
              </DialogClose>
              <Button type="submit" ref={buttonRef} disabled={submitting}>
                Adicionar
              </Button>
            </DialogFooter>
          </form>
        </Form>
      </DialogContent>
    </Dialog>
  )
}
