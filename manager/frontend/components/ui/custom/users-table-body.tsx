import { FC } from "react"
import { TableBody, TableCell, TableRow } from "../table"
import { UserDialog } from "./user-dialog"

interface UsersTableBody {
  filter: string
}

export const UsersTableBody: FC<UsersTableBody> = async ({ filter }) => {
  // TODO pass filter to backend
  await new Promise(resolve => setTimeout(resolve, 1000))
  const users = [
    { id: 1, name: "Pedro Mariano", email: "pedro.mariano@usp.br" },
    { id: 2, name: "Vinicius Lima", email: "viniciusflima@usp.br" }
  ]
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]
  const groups = [
    { id: 1, name: "Alunos", doors: [2] },
    { id: 2, name: "Funcionários", doors: [1] }
  ]

  return (
    <TableBody>
      {users
        .sort((a, b) => a.name.localeCompare(b.name))
        .map(user => (
          <UserDialog key={user.id} user={user} groups={groups} doors={doors}>
            <TableRow className="hover:cursor-pointer">
              <TableCell>{user.name}</TableCell>
              <TableCell>{user.email}</TableCell>
            </TableRow>
          </UserDialog>
        ))}
    </TableBody>
  )
}
