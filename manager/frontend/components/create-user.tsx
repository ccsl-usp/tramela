import { FC } from "react"
import { CreateUserDialog } from "./ui/custom/create-user-dialog"

export const CreateUser: FC = async () => {
  // TODO
  await new Promise(resolve => setTimeout(resolve, 1000))
  const doors = [
    { id: 1, name: "Lab XP" },
    { id: 2, name: "Lab SL" }
  ]
  const groups = [
    { id: 1, name: "Alunos", doors: [2] },
    { id: 2, name: "Funcionários", doors: [1] }
  ]

  return <CreateUserDialog doors={doors} groups={groups} />
}
