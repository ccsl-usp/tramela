import { FC } from "react"
import { Separator } from "./ui/separator"

interface LastAccessProps {
  className?: string
}

export const LastAccess: FC<LastAccessProps> = async ({ className }) => {
  // TODO
  await new Promise(resolve => setTimeout(resolve, 4000))
  const lastAccess = [
    { id: "1", user: "Foo", time: new Date(), action: "acessou" },
    { id: "2", user: "Bar", time: new Date(), action: "liberou" }
  ]

  return (
    <>
      {lastAccess.map(access => (
        <div key={access.id}>
          <div className="flex justify-between">
            <div key={access.id}>
              <span className="font-bold">{access.user}</span> {access.action}
            </div>

            <div>{access.time.toLocaleString()}</div>
          </div>

          <Separator className="my-2" />
        </div>
      ))}
    </>
  )
}
