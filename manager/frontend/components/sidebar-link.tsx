"use client"

import { FC, ReactNode } from "react"
import Link from "next/link"
import { usePathname } from "next/navigation"
import { cn } from "@/lib/utils"

interface SidebarLinkProps {
  href: string
  children: ReactNode
}

export const SidebarLink: FC<SidebarLinkProps> = ({ href, children }) => {
  const path = usePathname()

  return (
    <Link href={href} className={cn("hover:bg-primary rounded-lg mx-4 p-2", path === href ? "bg-primary" : "")}>
      {children}
    </Link>
  )
}
