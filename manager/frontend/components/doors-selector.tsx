"use client"

import { FC } from "react"
import { redirect, useRouter } from "next/navigation"
import { doorPage } from "@/routes"
import { Door } from "@/types/door"
import { cn } from "@/lib/utils"
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "./ui/select"

interface DoorsSelectorProps {
  doors: Door[]
  door?: Door
  className?: string
}

export const DoorsSelector: FC<DoorsSelectorProps> = ({ doors, door, className }) => {
  const router = useRouter()

  const handleSelectDoor = async (doorId: string) => {
    router.push(doorPage(doorId))
  }

  return (
    <Select onValueChange={handleSelectDoor} value={door?.id.toString()}>
      <SelectTrigger className={cn("w-80", className)}>
        <SelectValue placeholder="Selecione uma porta" />
      </SelectTrigger>

      <SelectContent>
        {doors.map(d => (
          <SelectItem key={d.id} value={d.id.toString()}>
            {d.name}
          </SelectItem>
        ))}
      </SelectContent>
    </Select>
  )
}
